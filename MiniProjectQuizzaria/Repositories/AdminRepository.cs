﻿using MiniProjectQuizzaria.Interfaces;
using MiniProjectQuizzaria.Models;
using PracticeEfCore;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace MiniProjectQuizzaria.Repositories
{
    public class AdminRepository : IAdmin
    {
        public void Addquestion(MyContext contextobj)
        {
            
            var quizzes = contextobj.Quizzes;
            int id = 0;
            Console.WriteLine("Select the id of quiz u want to enter the question for !!");
            foreach (Quiz item in quizzes)
            {
                Console.WriteLine("Quiz Id "+ item.QuizId+" Quiz Name "+item.QuizName);
            }
            id =Convert.ToInt32( Console.ReadLine());
 
            Question ques = new Question()
            {
                Ques = "Which is the prime number A. 49 B. 450 C. 500 D. 440",
                Answer = "A",
                QuizId = id
             };
            
            contextobj.Questions.Add(ques);
            contextobj.SaveChanges();
        }

        public void Addquiz(string category)
        {
            MyContext obj = new MyContext();
            //var cat = obj.Categories;
            int id = 0;
            var cat = (from c in obj.Categories where c.CategoryName == "Aptitude" select c);
            Console.WriteLine(cat.Count());
            foreach (Category item in cat)
            {
                if(item.CategoryName.Contains(category))
                {
                    id = item.CategoryId;
                }
            }
            
         if(id>0)
            {
                Quiz quiz = new Quiz();
                //quiz.quizid = 1;
                quiz.QuizName = "Number Systems";
                quiz.CategoryId = id;
                obj.Quizzes.Add(quiz);
                obj.SaveChanges();
            }
            else
            {
                Console.WriteLine("No category found");
            }
            
        }

        public void AddUser()
        {
            MyContext obj = new MyContext();
            User user = new User()
            {
                UserName="Adarsh",
                EmailId="adarsh@gmail.com",
                Password="adarsh",
                Phoneno="9039796187"
                 
            };
            obj.Users.Add(user);
            obj.SaveChanges();
            
        }

        public void DisplayUsers()
        {
            throw new NotImplementedException();
        }

        public void GiveTest(int userid)
        {
            QuizUser quizuserobj = new QuizUser();
            
            MyContext obj = new MyContext();
            var quizzes = obj.Quizzes;
            int id = 0;
            Console.WriteLine("Select the id of quiz u want gives !!");
            foreach (Quiz item in quizzes)
            {
                Console.WriteLine("Quiz Id " + item.QuizId + " Quiz Name " + item.QuizName);
            }

            id = Convert.ToInt32(Console.ReadLine());
            

            quizuserobj.QuizID = id;
            quizuserobj.UserId = userid;
            obj.QuizUser.Add(quizuserobj);
            obj.SaveChanges();

            //test started
            //score




        }
    }
}
