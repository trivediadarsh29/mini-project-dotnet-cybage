﻿using Microsoft.EntityFrameworkCore;
using MiniProjectQuizzaria.Models;

namespace PracticeEfCore
{
    public class MyContext : DbContext
    {
        public DbSet<Quiz> Quizzes { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Score> Scores { get; set; }
        public DbSet<Question> Questions { get; set;}
        public DbSet<Category> Categories { get; set;}
        public DbSet<QuizUser> QuizUser { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=DBQuizzaria1.1;Trusted_Connection=True;");
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<QuizUser>()
        .HasKey(bc => new { bc.QuizUserId });
            modelBuilder.Entity<QuizUser>()
                .HasOne(bc => bc.Quiz)
                .WithMany(b => b.QuizUsers)
                .HasForeignKey(bc => bc.QuizID);
            modelBuilder.Entity<QuizUser>()
                .HasOne(bc => bc._User)
                .WithMany(c => c.QuizUsers)
                .HasForeignKey(bc => bc.UserId);

            modelBuilder.Entity<Quiz>()
        .HasOne(e => e.Category)
        .WithMany(c => c.Quizzes)
                .OnDelete(DeleteBehavior.ClientNoAction);


         
            modelBuilder.Entity<Question>()
        .HasOne(e => e.Quiz)
        .WithMany(c => c.Questions)
                .OnDelete(DeleteBehavior.ClientNoAction);
        }
      

    }
}
