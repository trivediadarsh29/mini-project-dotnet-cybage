﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MiniProjectQuizzaria.Models
{
    public class Score
    {
        [Key]
        public int ScoreId { get; set;}
        public double Marks { get; set; }
        public int QuizId { get; set; }
        public Quiz Quiz { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}
