﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MiniProjectQuizzaria.Models
{
    public class User
    {
        [Key]
        public int UserId { get; set; }
        [Required]
        [MaxLength(30)]
        public string UserName { get; set; }
        [MaxLength(50)]
        [Required]
        [RegularExpression(@"^([A-Za-z0-9][^'!&\\#*$%^?<>()+=:;`~\[\]{}|/,₹€@ ][a-zA-z0-9-._][^!&\\#*$%^?<>()+=:;`~\[\]{}|/,₹€@ ]*\@[a-zA-Z0-9][^!&@\\#*$%^?<> ()+=':;~`.\[\]{}|/,₹€ ]*\.[a-zA-Z]{2,6})$", ErrorMessage = "Please enter a valid Email")]
        public string EmailId { get; set; }
        [MaxLength(10)]
        public string Phoneno { get; set; }
        [MaxLength(20)]
        [Required]
        public string Password { get; set; }
        [Required]
        public int Status { get; set; }
        [Required]
        public string  Role { get; set; }
        public Score _Score { get; set;}
        public ICollection<QuizUser> QuizUsers { get; set; }
    }
}
