﻿using MiniProjectQuizzaria.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Text;

namespace MiniProjectQuizzaria.Models
{
    public class Quiz
    {
        [Key]
        public int QuizId { get; set; }
        [Required]
        [MaxLength(30)]
        public string QuizName { get; set; }
        public Score  Score { get; set; }
        public ICollection<QuizUser> QuizUsers { get; set; }
        public List<Question> Questions { get; set; }
        public int? CategoryId { get; set; }
        public Category Category { get; set; }
    }
}
