﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MiniProjectQuizzaria.Models
{
    public class Question
    {
        [Key]
        public int QuesId { get; set;}
        [Required]
        
        public string Ques { get; set; }
        [Required]
        [MaxLength(5)]
        public string Answer { get; set; }
        public int? QuizId { get; set; }
        public Quiz Quiz { get; set; }

    }
}
