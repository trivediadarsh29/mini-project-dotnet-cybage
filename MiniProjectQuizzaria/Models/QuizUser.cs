﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MiniProjectQuizzaria.Models
{
    public class QuizUser
    {
        [Key]
        public int QuizUserId { get; set; }
        public int UserId { get; set; }
        public User _User { get; set; }
        public int QuizID { get; set; }
        public Quiz Quiz { get; set; }
    }
}
