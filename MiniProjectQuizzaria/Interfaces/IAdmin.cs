﻿using PracticeEfCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace MiniProjectQuizzaria.Interfaces
{
    interface IAdmin
    {
        void Addquiz(string category);
        void Addquestion(MyContext contextobj);
        void AddUser();
        void GiveTest(int userid);
        void DisplayUsers();
    }
}
